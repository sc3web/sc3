<?php
$events_selected = 1;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Care City Church</title>
    <?php include(__DIR__.'/../include/metatag.php'); ?>
    <link href="/events/events.css" rel="stylesheet">
  </head>
  <body>
    <?php include(__DIR__.'/../include/header.php'); ?>
    <div id="events-1-div" class="top-shadow-in">
      <div class="content">
        <h1 id="events-1-title">Upcoming Events</h1>
        <div id="events-1">
          <div>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </div>
          <div>
            <img src="/img/service-details.jpg" />
          </div>
        </div>
      </div>
    </div>
    <div class="content">
      <div id="events-list">
        <div><img src="/img/service-details.jpg" /></div>
        <div><img src="/img/service-details.jpg" /></div>
        <div><img src="/img/service-details.jpg" /></div>
        <div><img src="/img/service-details.jpg" /></div>
        <div><img src="/img/service-details.jpg" /></div>
        <div><img src="/img/service-details.jpg" /></div>
        <div><img src="/img/service-details.jpg" /></div>
        <div><img src="/img/service-details.jpg" /></div>
      </div>
    </div>
    <?php include(__DIR__.'/../include/footer.php'); ?>
  </body>
</html>
