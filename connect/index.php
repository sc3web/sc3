<?php
$connect_selected = 1;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Care City Church</title>
    <?php include(__DIR__.'/../include/metatag.php'); ?>
    <link href="/lib/slick/slick.css" rel="stylesheet">
    <link href="/lib/slick/slick-theme.css" rel="stylesheet">
    <script src="/lib/slick/slick.min.js"></script>
    <link href="/connect/connect.css" rel="stylesheet">
  </head>
  <body>
    <?php include(__DIR__.'/../include/header.php'); ?>
    <div id="connect-1-div" class="top-shadow-in">
      <div id="connect-1-a" class="content">
        <div id="connect-1" class="content">
        <div id="connect-list" class="slider">
          <div>
            <div class="divide-2">
              <div class="divide-left">
                <div class="connect-title">
                  Star Kids
                </div>
                <div class="connect-content">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                </div>
                <div class="connect-link">
                  <a href="/gallery/">GALLERY</a>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="/registration/">REGISTER</a>
                </div>
              </div>
              <div class="divide-right">
                <img src="/img/connect/star-kids.jpg" />
              </div>
            </div>
          </div>
          <div>
            <div class="divide-2">
              <div class="divide-left">
                <div class="connect-title">
                  Star Kids
                </div>
                <div class="connect-content">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                </div>
                <div class="connect-link">
                  <a href="/gallery/">GALLERY</a>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="/registration/">REGISTER</a>
                </div>
              </div>
              <div class="divide-right">
                <img src="/img/connect/star-kids.jpg" />
              </div>
            </div>
          </div>
          <div>
            <div class="divide-2">
              <div class="divide-left">
                <div class="connect-title">
                  Star Kids
                </div>
                <div class="connect-content">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                </div>
                <div class="connect-link">
                  <a href="/gallery/">GALLERY</a>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="/registration/">REGISTER</a>
                </div>
              </div>
              <div class="divide-right">
                <img src="/img/connect/star-kids.jpg" />
              </div>
            </div>
          </div>
          <div>
            <div class="divide-2">
              <div class="divide-left">
                <div class="connect-title">
                  Star Kids
                </div>
                <div class="connect-content">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                </div>
                <div class="connect-link">
                  <a href="/gallery/">GALLERY</a>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="/registration/">REGISTER</a>
                </div>
              </div>
              <div class="divide-right">
                <img src="/img/connect/star-kids.jpg" />
              </div>
            </div>
          </div>
        </div>
        <script>
          $('#connect-list').slick({
            arrows: true,
            slidesToShow: 1,
            autoplay: true,
            dots: true,
            autoplaySpeed: 10000,
          });
        </script>
        </div>
      </div>
    </div>
    <?php include(__DIR__.'/../include/footer.php'); ?>
  </body>
</html>
