<?php
$highlights_selected = 1;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Care City Church</title>
    <?php include(__DIR__.'/../include/metatag.php'); ?>
    <link href="/highlights/highlights.css" rel="stylesheet">
  </head>
  <body>
    <?php include(__DIR__.'/../include/header.php'); ?>
    <div id="highlights-main-div" class="top-shadow-in">
      <div id="highlights-main">
        <div id="highlights-date"><?=date('j F Y')?></div>
        <div id="highlights-content">
          <img src="/img/stand-by.png" />
        </div>
      </div>
    </div>
    <div class="content">
      <div id="highlights-verse">
        <img src="/img/doublequote-open.png" />
        <div id="highlights-verse-heading">Verse of the Week</div>
        <div id="highlights-verse-content">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </div>
        <img src="/img/doublequote-close.png" />
      </div>
    </div>
    <?php include(__DIR__.'/../include/footer.php'); ?>
  </body>
</html>
