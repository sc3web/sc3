<?php
$header_selected = '<img src="/img/header-selected.png" />';
?>
<div id="header-box">
  <div id="header">
    <div><a href="/" id="header-logo-a"><img id="header-logo" src="/img/logo-header.png" /></a></div>
    <div id="header-menu">
      <div><a href="/about/">ABOUT</a><?=isset($about_selected)?$header_selected:''?></div>
      <div><a href="/connect/">CONNECT</a><?=isset($connect_selected)?$header_selected:''?></div>
      <div><a href="/highlights/">HIGHLIGHTS</a><?=isset($highlights_selected)?$header_selected:''?></div>
      <div><a href="/events/">EVENTS</a><?=isset($events_selected)?$header_selected:''?></div>
      <div><a href="/gallery/">GALLERY</a><?=isset($gallery_selected)?$header_selected:''?></div>
      <div><a href="/registration/">REGISTRATION</a><?=isset($registration_selected)?$header_selected:''?></div>
      <div><a href="/contact/">CONTACT</a><?=isset($contact_selected)?$header_selected:''?></div>
    </div>
  </div>
</div>
