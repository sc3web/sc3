<div id="services-details-box">
  <div id="services-details">
    <div id="map"></div>
    <div id="service-details-text">
      <div>
        <div class="service-details-title">SERVICE DETAILS</div>
        <br />
        <div class="service-details-title">MAIN SERVICE</div>
        Sunday | 10 AM<br />
        <br />
        <div class="service-details-title">YOUTH SERVICE</div>
        Cellgroup : Wednesday | 7 PM<br />
        Worship : Saturday | 3 PM<br />
        <br />
        <div class="service-details-title">WOMEN IN CHRIST</div>
        Monday | 10 AM<br />
        <br />
        <div class="service-details-title">MEN IN CHRIST</div>
        Saturday | 10.30 AM<br />
        <br />
        <div class="service-details-title">SALTSHAKERS SERVICE</div>
        Saturday | 3 PM<br />
        <br />
        <div class="service-details-title">MAIN SERVICE LOCATION</div>
        Swissotel Hotel The Stamford, Atrium Ballroom, lv. 4<br />
        <br />
      </div>
    </div>
  </div>
</div>
<div id="footer">
  <div id="footer-content">
    <div><a href="/"><img id="footer-logo" src="/img/logo-footer.png" /></a></div>
    <div id="footer-social">
      <a href="https://www.facebook.com/"><img src="/img/footer/facebook.png" /></a>
      <a href="https://www.instagram.com/"><img src="/img/footer/instagram.png" /></a>
      <a href="https://plus.google.com/"><img src="/img/footer/plus.png" /></a>
    </div>
  </div>
</div>
<script>
function initMap() {
  var uluru = {lat: 1.2938398, lng: 103.8527933};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center: uluru,
    mapTypeControl: false
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: map
  });
}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBWQQ6GOfLT2jy292FDdfAzj47v9nlHkD8&amp;callback=initMap"></script>