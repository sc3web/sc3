<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="/lib/normalize.css" rel="stylesheet">
<link href="/lib/style.css" rel="stylesheet">
<link href="/lib/photoswipe/photoswipe.css" rel="stylesheet">
<link href="/lib/photoswipe/default-skin/default-skin.css" rel="stylesheet">
<script src="/lib/photoswipe/photoswipe.min.js"></script>
<script src="/lib/photoswipe/photoswipe-ui-default.min.js"></script>
<script src="/lib/jquery-3.2.1.slim.min.js"></script>
