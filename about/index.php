<?php
$about_selected = 1;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Care City Church</title>
    <?php include(__DIR__.'/../include/metatag.php'); ?>
    <link href="/lib/slick/slick.css" rel="stylesheet">
    <link href="/lib/slick/slick-theme.css" rel="stylesheet">
    <script src="/lib/slick/slick.min.js"></script>
    <link href="/about/about.css" rel="stylesheet">
  </head>
  <body>
    <?php include(__DIR__.'/../include/header.php'); ?>
    <div id="about-1-div" class="top-shadow-in">
      <div id="about-1" class="content">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
    </div>
    <div class="content">
      <div id="founders">
        <div id="founders-heading1">Our Founders</div>
        <div id="founders-selection" class="slider">
          <div>
            <img src="/upload/people/Lito.png" class="founder-img" />
            <div class="founders-heading-name">Ps. Lito Samsriwi</div>
          </div>
          <div>
            <img src="/upload/people/Maruly.png" class="founder-img" />
            <div class="founders-heading-name">Mr Maruly</div>
          </div>
          <div>
            <img src="/upload/people/Darmawan.png" class="founder-img" />
            <div class="founders-heading-name">Mr Darmawan</div>
          </div>
          <div>
            <img src="/upload/people/Lito.png" class="founder-img" />
            <div class="founders-heading-name">Ps. Lito Samsriwi</div>
          </div>
          <div>
            <img src="/upload/people/Maruly.png" class="founder-img" />
            <div class="founders-heading-name">Mr Maruly</div>
          </div>
          <div>
            <img src="/upload/people/Darmawan.png" class="founder-img" />
            <div class="founders-heading-name">Mr Darmawan</div>
          </div>
          <div>
            <img src="/upload/people/Lito.png" class="founder-img" />
            <div class="founders-heading-name">Ps. Lito Samsriwi</div>
          </div>
          <div>
            <img src="/upload/people/Maruly.png" class="founder-img" />
            <div class="founders-heading-name">Mr Maruly</div>
          </div>
          <div>
            <img src="/upload/people/Darmawan.png" class="founder-img" />
            <div class="founders-heading-name">Mr Darmawan</div>
          </div>
        </div>
        <script>
          $('#founders-selection').slick({
            arrows: true,
            centerMode: true,
            centerPadding: '100px',
            slidesToShow: 3,
            autoplay: true,
            autoplaySpeed: 3000,
            responsive: [
              {
                breakpoint: 768,
                settings: {
                  arrows: false,
                  centerMode: true,
                  centerPadding: '40px',
                  slidesToShow: 3
                }
              },
              {
                breakpoint: 480,
                settings: {
                  arrows: false,
                  centerMode: true,
                  centerPadding: '40px',
                  slidesToShow: 1
                }
              }
            ]
          });
        </script>
        <img id="founders-quote-open" src="/img/doublequote2-open.png" />
        <div id="quote-content">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </div>
        <img id="founders-quote-close" src="/img/doublequote2-close.png" />
        <img src="/img/about/care.png" style="padding:30px;" />
        <img src="/img/about/vision-mission.png" style="padding:30px;" />
      </div>
    </div>
    <?php include(__DIR__.'/../include/footer.php'); ?>
  </body>
</html>
