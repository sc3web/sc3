<?php
$contact_selected = 1;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Care City Church</title>
    <?php include(__DIR__.'/../include/metatag.php'); ?>
    <link href="/contact/contact.css" rel="stylesheet">
  </head>
  <body>
    <?php include(__DIR__.'/../include/header.php'); ?>
    <div id="contact-div" class="top-shadow-in">
      <div id="contact-split2-div">
        <div id="contact-split2">
          <div id="contact-left">
            <div id="contact-title">Contact Us</div>
            <div class="contact-content">
              We are here to answer any questions you may have, reach out to us
              <br />
              and we will respond as soon as we can.
            </div>
            <div style="height:20px;"></div>
            <form action="" method="POST">
              <div class="form-row">
                <div>Name</div>
                <div><input type="text" autofocus /></div>
              </div>
              <div class="form-row">
                <div>E-mail</div>
                <div><input type="text" /></div>
              </div>
              <div id="message-title">Message</div>
              <textarea id="message-textarea" rows="5"></textarea>
              <button type="submit" id="contact-submit">SUBMIT</button>
            </form>
          </div>
          <div id="contact-right">
            <div>
              <a href="tel:+6594389909" class="contact-right-link">
                <div><img src="/img/contact/phone.png" /></div>9438 9909
              </a>
            </div>
            <div style="height:50px;"></div>
            <div>
              <a href="mailto:carecitychurch.sg@gmail.com" class="contact-right-link">
                <div><img src="/img/contact/email.png" /></div>carecitychurch.sg@gmail.com
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include(__DIR__.'/../include/footer.php'); ?>
  </body>
</html>
