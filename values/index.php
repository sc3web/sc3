<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Care City Church</title>
    <?php include(__DIR__.'/../include/metatag.php'); ?>
  </head>
  <body>
    <?php include(__DIR__.'/../include/header.php'); ?>
    <div class="content">
      <h1>Values</h1>
      <div class="horizontal-line"></div>
    </div>
    <?php include(__DIR__.'/../include/footer.php'); ?>
  </body>
</html>
