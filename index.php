<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Care City Church</title>
    <?php include(__DIR__.'/include/metatag.php'); ?>
    <link href="/lib/home.css" rel="stylesheet">
  </head>
  <body>
    <?php include(__DIR__.'/include/header.php'); ?>
    <div id="main-banner-div">
      <div id="main-banner-shadow"></div>
      <img id="main-banner" src="/img/home/banner1.jpg" />
    </div>
    <div id="home-service-group-div">
      <div id="home-service-group">
        <div>
          <img src="/img/home/kids.png" class="img-circle" />
          <div class="home-service-group-title">Star Kids</div>
          <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
        </div>
        <div>
          <img src="/img/home/youth.png" class="img-circle" />
          <div class="home-service-group-title">Excellent Youth</div>
          <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
        </div>
        <div>
          <img src="/img/home/adult.png" class="img-circle" />
          <div class="home-service-group-title">Adult</div>
          <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
        </div>
      </div>
    </div>
    <?php include(__DIR__.'/include/footer.php'); ?>
  </body>
</html>
