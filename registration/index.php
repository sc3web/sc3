<?php
$registration_selected = 1;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Care City Church</title>
    <?php include(__DIR__.'/../include/metatag.php'); ?>
    <link href="/registration/registration.css" rel="stylesheet">
</head>
  <body>
    <?php include(__DIR__.'/../include/header.php'); ?>
    <div id="registration-div">
      <div class="content">
        <div id="registration-title">Shalom,</div>
        <div id="registration-text">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
        </div>
        <div id="registration-split2-div">
          <div id="registration-split2">
            <div id="registration-left">
              <form action="" method="POST">
                <div class="form-row">
                  <div>Name</div>
                  <div><input type="text" autofocus /></div>
                </div>
                <div class="form-row">
                  <div>Age</div>
                  <div><input type="number" min="1" max="99" /></div>
                </div>
                <div class="form-row">
                  <div>B.O.D.</div>
                  <div><input type="date" /></div>
                </div>
              </form>
            </div>
            <div id="registration-right">
              <div class="form-two">
                <div style="width: 160px;">Gender</div>
                <div class="radio-group">
                  <div class="radio"><div>F</div><input type="radio" name="gender" value="F" checked /><div id="slash">/</div></div>
                  <div class="radio"><div>M</div><input type="radio" name="gender" value="M" /></div>
                </div>
              </div>
              <div class="form-two" id="phone-number">
                <div style="width: 160px;">Phone Number</div>
                <div style="display: flex; flex-grow: 1; background-color: #ffffff; align-items: center; border-radius: 7px;">
                  <select id="country" name="country" style="width: auto;"></select>
                  <div style="width: auto; padding-top: 12px; padding-bottom: 0;">&nbsp; | &nbsp;&nbsp;</div>
                  <input type="text" name="phone" style="flex-grow: 1;" />
                </div>
              </div>
              <button type="submit" id="registration-submit">REGISTER</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script>
      for (var i=1; i<100; i++) {
        var option = document.createElement('option');
        option.text = '+'+i;
        option.value = '+'+i;
        if (i == 65) {
          option.selected = true;
        }
        document.querySelector('#country').append(option);
      }
    </script>
    <?php include(__DIR__.'/../include/footer.php'); ?>
  </body>
</html>
